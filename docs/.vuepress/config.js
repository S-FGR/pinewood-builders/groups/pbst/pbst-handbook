module.exports = ctx => ({
  title: 'PBST Handbook',
  description: 'An unofficial PBST Handbook, kept mostly up to date!',
  themeConfig: {
    repo: 'https://gitlab.com/pinewood-builders/pbst-handbook',
    editLinks: true,
    docsDir: 'docs/',
    logo: '/PBST-Logo.png',
    smoothScroll: true,
    sidebarDepth: 1,
    yuu: {
      defaultDarkTheme: true,
      disableThemeIgnore: true,
      logo: 'PBST-Logo',
    },
    algolia: ctx.isProd ? ({
      apiKey: '335b94bd05315a8ed572b77d95e6d5b7',
      indexName: 'pbst'
    }) : null,

    nav: [
      {
        text: 'Home',
        link: '/'
      },
      {
        text: 'PBST Handbook',
        link: '/pbst/handbook/handbook.html'
      },
      {
        text: 'Pinewood',
        items: [{
          text: 'Pinewood Homepage',
          link: 'https://pinewood-builders.com'
        },
          {
            text: 'TMS Handbook',
            link: 'https://tms.pinewood-builders.com'
          },
          {
            text: 'PET Handbook',
            link: 'https://pet.pinewood-builders.com'
          }
        ]
      }
    ],

    sidebar: [{
      collapsable: true,
      title: 'PBST Handbook',
      children: ['/pbst/handbook/handbook'],
    },
    ],
    
  },
  head: [
    ['link', {
      rel: 'icon',
      href: '/PBST-Logo.png'
    }],
    ['link', {
      rel: 'manifest',
      href: '/manifest.json'
    }],
    ['meta', {
      name: 'theme-color',
      content: '#1068bf'
    }], 
    ['<script type="application/javascript"> (function(b,o,n,g,s,r,c){if(b[s])return;b[s]={};b[s].scriptToken="XzE2OTQ5MjMxMg";b[s].callsQueue=[];b[s].api=function(){b[s].callsQueue.push(arguments);};r=o.createElement(n);c=o.getElementsByTagName(n)[0];r.async=1;r.src=g;r.id=s+n;c.parentNode.insertBefore(r,c);})(window,document,"script","https://cdn.oribi.io/XzE2OTQ5MjMxMg/oribi.js","ORIBI"); </script>']
  ],
  plugins: [
    ['@vuepress/pwa',
      {
        serviceWorker: true,
        updatePopup: true
      }
    ],
    [
      '@vuepress/google-analytics',
      {
        'ga': 'UA-168777162-2' // UA-00000000-0
      }
    ],
    [
      'vuepress-plugin-copyright',
      {
        noCopy: true, // the selected text will be uncopiable
        minLength: 100, // if its length is greater than 100
      },
    ],
  ],
})
